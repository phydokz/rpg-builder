app.controller('AnimationsController',['$rootScope','URLS','$http',function($scope,URLS,$http){
    var self = this;
    $scope.animations = [];
    $scope.modalNewAnimation = false;
    $scope.animation = {
        name:''
    };

    $scope.init = function(){
        $scope.page.title = 'Animações';
        self.loadAnimations();
    };

    $scope.removeAnimation = function(id){
        $http({
            method:'DELETE',
            params:{
                id:id
            },
            url:URLS.BASE_URL+'animations/delete',
            withCredentials:true

        }).then(function(response){
            if(response.data.success){
                $scope.animations.forEach(function(animation,index){
                    if(animation._id === id){
                        $scope.animations.splice(index,1);
                        return false;
                    }
                });
            }
        },function(error){

        });
    };

    $scope.createAnimation = function(){
        var string = JSON.stringify({
            speed:5,
            frames:[]
        });
        string = LZString.compressToUTF16(string);
        $http({
            method:'POST',
            url:URLS.BASE_URL+'animations',
            params:{
                name:$scope.nome
            },
            data:{
                animation:string,
                name:$scope.animation.name
            },
            withCredentials:true
        }).then(function(response){
            if(response.data.success){
                $scope.modalNewAnimation = false;
                $scope.animations.push(response.data.animation);
            }
            else{
                $scope.errors =response.data;
            }
        },function(error){

        });
    };

    $scope.addAnimation = function(){
        $scope.modalNewAnimation = true;
    };

    $scope.hideNewAnimationModal = function(){
        $scope.modalNewAnimation = false;
    };

    self.loadAnimations = function(){
        $http({
            method:'get',
            url:URLS.BASE_URL+'animations/list',
            withCredentials:true
        }).then(function(response){
            $scope.animations = response.data.animations;
        },function(){

        });
    };
}]);
