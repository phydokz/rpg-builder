app.controller('MapsController',['$rootScope','URLS','$http',function($scope,URLS,$http){
    var self = this;
    $scope.maps = [];
    $scope.modalNewMap = false;
    $scope.map = {
        name:'',
        width:20,
        height:20
    };

    $scope.init = function(){
        $scope.page.title = 'Mapas';
        self.loadMaps();
    };

    $scope.removeMap = function(id){
        $http({
            method:'DELETE',
            params:{
                id:id
            },
            url:URLS.BASE_URL+'maps/delete',
            withCredentials:true

        }).then(function(response){
            if(response.data.success){
                $scope.maps.forEach(function(map,index){
                    if(map._id === id){
                        $scope.maps.splice(index,1);
                        return false;
                    }
                });
            }
        },function(error){

        });
    };

    $scope.createMap = function(){
        var string = JSON.stringify({
            width:$scope.map.width,
            height:$scope.map.height
        });
        string = LZString.compressToUTF16(string);

        $http({
            method:'POST',
            url:URLS.BASE_URL+'maps',
            params:{
                name:$scope.nome
            },
            data:{
                map:string,
                name:$scope.map.name
            },
            withCredentials:true
        }).then(function(response){
            if(response.data.success){
                $scope.modalNewMap = false;
                $scope.maps.push(response.data.map);
            }
            else{
                $scope.errors =response.data;
            }
        },function(error){

        });
    };

    $scope.addMap = function(){
        $scope.modalNewMap = true;
    };

    $scope.hideNewMapModal = function(){
        $scope.modalNewMap = false;
    };

    self.loadMaps = function(){
        $http({
            method:'get',
            url:URLS.BASE_URL+'maps/list',
            withCredentials:true
        }).then(function(response){
            $scope.maps = response.data.maps;
        },function(){

        });
    };
}]);
