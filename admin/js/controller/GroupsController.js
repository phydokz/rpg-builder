app.controller('GroupsController',['$rootScope','URLS','$http',function($scope,URLS,$http){
    var self = this;
    $scope.groups = [];
    $scope.modal = false;
    $scope.group = {
        name:'',
        width:10,
        height:10
    };

    $scope.init = function(){
        $scope.page.title = 'Grupos';
        self.load();
    };

    $scope.remove = function(id){
        $http({
            method:'DELETE',
            params:{
                id:id
            },
            url:URLS.BASE_URL+'groups/delete',
            withCredentials:true

        }).then(function(response){
            if(response.data.success){
                $scope.groups.forEach(function(group,index){
                    if(group._id === id){
                        $scope.groups.splice(index,1);
                        return false;
                    }
                });
            }
        },function(error){

        });
    };

    $scope.create = function(){
        var string = JSON.stringify({
            width:$scope.group.width,
            height:$scope.group.height
        });
        string = LZString.compressToUTF16(string);

        $http({
            method:'POST',
            url:URLS.BASE_URL+'groups',
            params:{
                name:$scope.nome
            },
            data:{
                group:string,
                name:$scope.group.name
            },
            withCredentials:true
        }).then(function(response){
            if(response.data.success){
                $scope.modal = false;
                $scope.groups.push(response.data.group);
            }
            else{
                $scope.errors =response.data;
            }
        },function(error){

        });
    };

    $scope.add = function(){
        $scope.modal = true;
    };

    $scope.hideModal = function(){
        $scope.modal = false;
    };

    self.load = function(){
        $http({
            method:'get',
            url:URLS.BASE_URL+'groups/list',
            withCredentials:true
        }).then(function(response){
            $scope.groups = response.data.groups;
        },function(){

        });
    };
}]);