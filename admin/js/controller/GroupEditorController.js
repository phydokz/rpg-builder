app.controller('GroupEditorController',['$location', '$rootScope', '$localStorage', 'ImageLoader', '$timeout', '$http', 'URLS', '$stateParams', 'Utils', function($location,$scope,$storage,ImageLoader,$timeout,$http,URLS,$stateParams){
    var self = this;
    $scope.storage = $storage;
    $scope.group = null;
    self.groupCanvas = null;
    self.tilesetCanvas = null;
    self.tilesetLayer = null;
    self.selectedInterval = null;
    $scope.tmp_group = null;
    $scope.errors = null;
    $scope.currentLayer = 0;
    $scope.layers = [];
    $scope.grid = {
        active:true
    };
    $scope.rows = [];
    $scope.graphic = {
        gridColor:'#FFFFFF'
    };

    $scope.tilesetData = {
        graphic:{
            gridColor:'#FFFFFF',
            rows:1,
            cols:1,
            imageData:{
                url:''
            },
            image:null
        }
    };
    $scope.selectedTool = 'pencil';
    $scope.modalVisible = false;
    $scope.loaded = false;

    $scope.showModal = function(){
        $scope.modalVisible = true;
    };

    $scope.hideModal = function(){
        $scope.modalVisible = false;
    };

    $scope.init = function(){
        $scope.page.title = 'Editor de Groupas';
        self.loadGroup($stateParams.name,function() {
            $http({
                method: 'GET',
                url: URLS.BASE_URL+'groups/'+$scope.group.file,
                withCredentials: true,
                header : {'Content-Type' : 'application/text; charset=UTF-8'},
                transformResponse: function (data) {
                    return data;
                }
            }).then(function (response) {
                var json_string = LZString.decompressFromUTF16(response.data);
                var json = JSON.parse(json_string);
                self.initializeGroup();
                self.initializeTileset();
                self.loadGroupData(json);
                $scope.loaded = true;
            }, function (error) {
                console.error(error);
            });
        });
    };

    $scope.$watch('grid.active',function(newVal,oldVal){
        if(newVal !== oldVal){
            self.getGroupCanvas().getGridLayer().set({
                opacity:newVal?1:0
            });
        }
    });

    self.loadGroupData = function(data){
        var tmp_group = self.getGroup();
        var tile_w = parseInt(data.tile_w);
        var tile_h = parseInt(data.tile_h);
        tile_w = isNaN(tile_w)?32:tile_w;
        tile_h = isNaN(tile_h)?32:tile_h;
        tmp_group.tile_w = tile_w;
        tmp_group.tile_h = tile_h;
        var tilesets = data.tilesets !== undefined?data.tilesets:[];
        var tiles = data.tiles !== undefined?data.tiles:[];

        ImageLoader.loadAll(tilesets,function(tilesets){
            tiles.forEach(function(row,indexA){
                if(row !== null){
                    row.forEach(function(col,indexB){
                        if(col !== null){
                            col.forEach(function(tile,layer){
                                if(tile !== null){
                                    var newtile = {};
                                    CE.EXT.Map.tile_fields.forEach(function(name,index){
                                        if(tile[index] !== undefined){
                                            newtile[name] = tile[index];
                                        }
                                    });

                                    if(newtile.image !== undefined){
                                        var id = newtile.image;
                                        if(tilesets[id] !== undefined){
                                            newtile.image = tilesets[id];
                                        }
                                    }

                                    tmp_group.setTile(indexA,indexB,newtile);
                                }
                                else{
                                    tmp_group.removeTile(indexA,indexB);
                                }
                            });
                        }
                    });
                }
            });
            $scope.changeGroupWidth(parseInt(data.width));
            $scope.changeGroupHeight(parseInt(data.height));
        });
    };

    self.initializeGroup = function(){
        var groupCanvas = self.getGroupCanvas();
        var grid = new CE.AbstractGrid({
            sw:32,
            sh:32
        });

        var layer = groupCanvas.getGridLayer();
        layer.setGrid(grid);
        layer.refresh();


        $scope.layers[0] = groupCanvas.createLayer();
        for(var i = 0; i < 10;i++){
            $scope.rows[i] = 10-i;
        }

        groupCanvas.getMouseReader().onmousemove(function(){
            var reader = this;
            if(reader.right){
                var gridLayer =  groupCanvas.getGridLayer();
                gridLayer.refresh();
                self.getGroup().draw($scope.layers,gridLayer.getVisibleArea());
            }
        });

    };

    self.initializeTileset = function(){
        var tilesetCanvas = self.getTilesetCanvas();
        self.tilesetLayer = tilesetCanvas.createLayer({
            name:'tileset-layer'
        });
        var layer = tilesetCanvas.getGridLayer();
        layer.set({
            opacity:0.8
        });

        layer.refresh();
    };

    $scope.changeGraphic = function(url){
        $scope.modalVisible = false;
        $scope.tilesetData.graphic.imageData.url = url;
    };

    $scope.changeGroupWidth = function(width){
        var groupCanvas = self.getGroupCanvas();
        var group  = self.getGroup();
        group.width = width;
        groupCanvas.set({
            width:group.tile_w*width
        });
        groupCanvas.applyToLayers({
            width:group.tile_w*width
        });
        var gridLayer = groupCanvas.getGridLayer();
        gridLayer.getGrid().set({
            width:group.tile_w*width
        });
        gridLayer.refresh();
        group.draw($scope.layers,gridLayer.getVisibleArea());
    };



    $scope.changeGroupHeight = function(height){
        var groupCanvas = self.getGroupCanvas();
        var group = self.getGroup();
        $scope.rows.length = height;

        group.height = height;
        groupCanvas.applyToLayers({
            height:group.tile_h*height
        });

        var gridLayer = groupCanvas.getGridLayer();
        gridLayer.getGrid().set({
            height:group.tile_h*height
        });
        gridLayer.refresh();
        group.draw($scope.layers,gridLayer.getVisibleArea());
    };

    $scope.$watch('tilesetData.graphic.imageData.url',function(newVal, oldVal){
        $timeout(function(){
            if(newVal !== oldVal && self.tilesetLayer !== null){
                ImageLoader.load(newVal,function(img){
                    $scope.tilesetData.graphic.image = img;
                    var tilesetImage = self.getTilesetCanvas();

                    tilesetImage.set({
                        viewX:0,
                        viewY:0
                    });

                    tilesetImage.applyToLayers({
                        width:img.width,
                        height:img.height
                    });

                    self.tilesetLayer.clear().drawImage(img,0,0);
                    var tilesetGridLayer =  tilesetImage.getGridLayer();


                    tilesetGridLayer.getGrid().set({
                        width:img.width,
                        height:img.height,
                        sw:img.width,
                        sh:img.height
                    });
                    tilesetGridLayer.refresh();

                    $timeout(function(){
                        var rows = Math.floor(img.height/32);
                        var cols = Math.floor(img.width/32);
                        rows = rows < 1?1:rows;
                        cols = cols < 1?1:cols;
                        $scope.tilesetData.graphic.rows = rows;
                        $scope.tilesetData.graphic.cols = cols;
                        $scope.changeRows(rows);
                        $scope.changeCols(cols);
                    });

                });

            }
        });
    });

    $scope.changeRows = function(val){
        var image = $scope.tilesetData.graphic.image;
        if(image !== null){
            var layer = self.getTilesetCanvas().getGridLayer();
            layer.getGrid().set({
                sh:image.height/val
            });
            layer.refresh();
        }
    };

    $scope.changeCols = function(val){
        var image = $scope.tilesetData.graphic.image;
        if(image !== null){
            var layer = self.getTilesetCanvas().getGridLayer();
            layer.getGrid().set({
                sw:image.width/val
            });
            layer.refresh();
        }
    };

    $scope.export = function(){
        console.log(JSON.stringify(self.getGroup().toObject()));
    };

    /*Canvas onde o groupa é renderizado*/
    self.getGroupCanvas = function(){
        if(self.groupCanvas === null){
            self.groupCanvas = CE.createEngine({
                container:'#canvas-group',
                width:320,
                height:320,
                draggable:true
            },CE.CanvasEngineGrid);


            var reader = self.groupCanvas.getMouseReader();

            reader.onmousedown(MouseReader.LEFT,function(){
                var reader = this;
                var groupCanvas = self.getGroupCanvas();
                var group = self.getGroup();
                var layer = $scope.layers[$scope.currentLayer];
                var pos = groupCanvas.getPosition(reader.lastDown.left);
                var i = Math.floor(pos.y/group.tile_h);
                var j = Math.floor(pos.x/group.tile_w);
                var tile = {
                    width:group.tile_w,
                    height:group.tile_h
                };

                switch($scope.selectedTool){
                    case 'pencil':
                        if(self.selectedInterval !== null){
                            if(self.selectedInterval.type === 'eyedropper'){
                                tile = _.clone(self.selectedInterval);
                                tile.layer = $scope.currentLayer;
                                tile.dx = j*group.tile_w;
                                tile.dy = i*group.tile_h;

                                layer.clearRect({
                                    x:tile.dx,
                                    y:tile.dy,
                                    width:tile.dWidth,
                                    height:tile.dHeight
                                });
                                layer.image(tile);
                                group.setTile(i,j,tile);
                            }
                            else{
                                var image = $scope.tilesetData.graphic.image;
                                var interval = self.selectedInterval;
                                tile.sx = group.tile_w*interval.sj;
                                tile.sy = group.tile_h*interval.si;
                                tile.dx = group.tile_w*j;
                                tile.dy = group.tile_h*i;
                                tile.image = image;
                                tile.dWidth = group.tile_w;
                                tile.dHeight = group.tile_h;
                                tile.sWidth = group.tile_w;
                                tile.sHeight = group.tile_h;
                                tile.layer = $scope.currentLayer;
                                layer.clearRect({
                                    x:tile.dx,
                                    y:tile.dy,
                                    width:tile.dWidth,
                                    height:tile.dHeight
                                });
                                layer.image(tile);
                                group.setTile(i,j,tile);
                            }
                        }
                        break;
                    case 'eraser':
                        tile.x = group.tile_w*j;
                        tile.y = group.tile_h*i;
                        layer.clearRect(tile);
                        group.removeTile(i,j,$scope.currentLayer);
                        break;
                    case 'eyedropper':
                        tile = group.getTile(i,j,$scope.currentLayer);
                        tile = _.clone(tile);
                        tile.type = 'eyedropper';
                        self.selectedInterval = tile;

                }

            });

            reader.onmousemove(function(e){
                var reader = this;
                var groupCanvas = self.getGroupCanvas();
                var group = self.getGroup();
                var grid_layer = groupCanvas.getGridLayer();
                var pos = groupCanvas.getPosition(reader.lastMove);
                var x = Math.floor(pos.x/group.tile_w)*group.tile_w;
                var y = Math.floor(pos.y/group.tile_h)*group.tile_h;
                grid_layer.getGrid().setCheckedArea({
                    x:x,
                    y:y,
                    width:group.tile_w,
                    height:group.tile_h
                });
                grid_layer.refresh();
                var i, j;
                if(reader.left){
                    var interval,area_interval,col,row;
                    var layer = $scope.layers[$scope.currentLayer];

                    switch($scope.selectedTool){
                        case 'pencil':
                            if(self.selectedInterval !== null){
                                interval = self.selectedInterval;
                                var area = groupCanvas.getDrawedArea();
                                area_interval = group.getAreaInterval(area);

                                if(self.selectedInterval.type === 'eyedropper'){
                                    tile = _.clone(self.selectedInterval);
                                    delete tile.type;
                                    for(i = area_interval.si;i <= area_interval.ei;i++){
                                        for(j = area_interval.sj; j <= area_interval.ej;j++){
                                            tile.dx = j*group.tile_w;
                                            tile.dy = i*group.tile_h;
                                            tile.layer = $scope.currentLayer;
                                            layer.clearRect({
                                                x:tile.dx,
                                                y:tile.dy,
                                                width:group.tile_w,
                                                height:group.tile_h
                                            });

                                            layer.image(tile);
                                            group.setTile(i,j,tile);
                                        }
                                    }
                                }
                                else{
                                    var image = $scope.tilesetData.graphic.image;
                                    for(i = area_interval.si,row=interval.si;i <= area_interval.ei;i++){
                                        for(j = area_interval.sj,col=interval.sj; j <= area_interval.ej;j++){
                                            x = j*group.tile_w;
                                            y = i*group.tile_h;

                                            var tile = {
                                                image:image,
                                                dWidth:group.tile_w,
                                                dHeight:group.tile_h,
                                                sWidth:group.tile_w,
                                                sHeight:group.tile_h,
                                                sx:col*group.tile_w,
                                                sy:row*group.tile_h,
                                                dx:x,
                                                dy:y,
                                                layer:$scope.currentLayer
                                            };

                                            layer.clearRect({
                                                x:x,
                                                y:y,
                                                width:group.tile_w,
                                                height:group.tile_h
                                            });

                                            layer.image(tile);
                                            group.setTile(i,j,tile);
                                            col++;
                                            if(col > interval.ej){
                                                col = interval.sj;
                                            }
                                        }
                                        row++;
                                        if(row > interval.ei){
                                            row = interval.si;
                                        }
                                    }
                                }
                            }

                            break;
                        case 'eraser':
                            area_interval = group.getAreaInterval({x:pos.x,y:pos.y,width:1,height:1});
                            for(i = area_interval.si;i <= area_interval.ei;i++){
                                for(j = area_interval.sj; j <= area_interval.ej;j++){
                                    x = j*group.tile_w;
                                    y = i*group.tile_h;

                                    layer.clearRect({
                                        x:x,
                                        y:y,
                                        width:group.tile_w,
                                        height:group.tile_h
                                    });
                                    group.removeTile(i,j,$scope.currentLayer);
                                }
                            }
                    }
                }
            });
        }
        return self.groupCanvas;
    };

    /*Tileset Canvas*/
    self.getTilesetCanvas = function(){
        if(self.tilesetCanvas === null){
            self.tilesetCanvas = CE.createEngine({
                container:'#tileset',
                width:520,
                height:520,
                selectable:true,
                draggable:true,
                multiSelect:true
            },CE.CanvasEngineGrid);


            self.tilesetCanvas.onAreaSelect(function(area,grid){
                var reader = this.getMouseReader();
                if(reader.left){
                    var rectSets = grid.getRectsFromArea(area);
                    var interval = grid.getAreaInterval(area);
                    grid.apply({
                        fillStyle:'transparent',
                        state:0
                    });
                    rectSets.forEach(function(rectSet){
                        rectSet.set({
                            fillStyle:'rgba(0,0,100,0.5)',
                            state:1
                        });
                    });
                    self.selectedInterval = interval;
                }
                else{
                    var rects = grid.getRectsFromArea(area);
                    if(rects.length > 0){
                        var rectSet = rects[0];
                        grid.apply({
                            fillStyle:'transparent'
                        },function(){
                            return this.state != 1;
                        });
                        rectSet.set({
                            fillStyle:'rgba(0,0,100,0.5)'
                        });
                    }
                }
            });
        }
        return self.tilesetCanvas;
    };

    self.getGroup = function(){
        var self = this;
        if($scope.tmp_group === null){
            var groupCanvas = self.getGroupCanvas();
            $scope.tmp_group = new CE.Group({
                sw:32,
                sh:32,
                width:$scope.group.width,
                height:$scope.group.height
            });
        }
        return $scope.tmp_group;
    };

    $scope.selectTool = function(tool){
        $scope.selectedTool = tool;
    };

    $scope.showLayers = function(){
        $scope.layers.forEach(function(layer){
            layer.set({
                opacity:1
            });
        });
    };

    $scope.save = function(){
        var json = JSON.stringify(self.getGroup().toObject());
        json = LZString.compressToUTF16(json);

        $http({
            method:'POST',
            url:URLS.BASE_URL+'groups/update',
            data:{
                id:$scope.group._id,
                group:json
            },
            withCredentials:true
        }).then(function(response){
            if(response.data.success){
                console.log('groupa atualizado!');
            }
            else{
                console.log(response.data);
            }
        },function(error){
            console.log(error);
        });
    };

    self.loadGroup = function(name,success,error){
        $http({
            method:'GET',
            url:URLS.BASE_URL+'groups/load',
            params:{
                name:name
            },
            withCredentials:true
        }).then(function(response){
            if(response.data.success){
                $scope.group = response.data.group;
                $timeout(function(){
                    success();
                });
            }
            else{
                error();
            }
        },function(){
            error();
        });
    };
}]);
