app.controller('MapEditorController',
    ['$location', '$rootScope', '$localStorage', 'ImageLoader', '$timeout', '$http', 'URLS', '$stateParams', 'Utils',
        function($location,$scope,$storage,ImageLoader,$timeout,$http,URLS,$stateParams,Utils){
            var self = this;
            $scope.storage = $storage;
            $scope.map = null;
            self.mapCanvas = null;
            self.tilesetCanvas = null;
            self.tilesetLayer = null;
            self.selectedInterval = null;
            self.colisionLayers = [];
            $scope.tmp_map = null;
            $scope.errors = null;
            $scope.currentLayer = 0;
            $scope.layers = [];

            $scope.grid = {
                active:true
            };
            $scope.graphic = {
                gridColor:'#FFFFFF'
            };

            $scope.tilesetData = {
                graphic:{
                    gridColor:'#FFFFFF',
                    rows:1,
                    cols:1,
                    imageData:{
                        url:''
                    },
                    image:null
                }
            };
            $scope.selectedTool = 'pencil';
            $scope.modalVisible = false;
            $scope.loaded = false;
            $scope.mode = 'map';
            $scope.selectedEvent = null;

            $scope.showModal = function(){
                $scope.modalVisible = true;
            };

            $scope.hideModal = function(){
                $scope.modalVisible = false;
            };

            $scope.init = function(){
                $scope.page.title = 'Editor de Mapas';
                self.loadMap($stateParams.name,function() {
                    $http({
                        method: 'GET',
                        url: URLS.BASE_URL+'maps/'+$scope.map.file,
                        withCredentials: true,
                        header : {'Content-Type' : 'application/text; charset=UTF-8'},
                        transformResponse: function (data) {
                            return data;
                        }
                    }).then(function (response) {
                        var json_string = LZString.decompressFromUTF16(response.data);
                        var json = JSON.parse(json_string);
                        self.initializeMap();
                        self.initializeTileset();
                        self.loadMapData(json);
                        $scope.loaded = true;
                    }, function (error) {
                        console.error(error);
                    });
                });
            };

            $scope.$watch('grid.active',function(newVal,oldVal){
                if(newVal !== oldVal){
                    self.getMapCanvas().getGridLayer().set({
                        opacity:newVal?1:0
                    });
                }
            });


            self.loadMapData = function(data){
                var tmp_map = self.getMap();
                var tile_w = parseInt(data.tile_w);
                var tile_h = parseInt(data.tile_h);
                tile_w = isNaN(tile_w)?32:tile_w;
                tile_h = isNaN(tile_h)?32:tile_h;
                tmp_map.tile_w = tile_w;
                tmp_map.tile_h = tile_h;
                var tilesets = data.tilesets !== undefined?data.tilesets:[];
                var tiles = data.tiles !== undefined?data.tiles:[];
                ImageLoader.loadAll(tilesets,function(tilesets){
                    tiles.forEach(function(row,indexA){
                        if(row !== null){
                            row.forEach(function(col,indexB){
                                if(col !== null){
                                    col.forEach(function(tile,layer){
                                        if(tile !== null){
                                            var newtile = {};
                                            CE.Map.tile_fields.forEach(function(name,index){
                                                if(tile[index] !== undefined){
                                                    newtile[name] = tile[index];
                                                }
                                            });

                                            if(newtile.image !== undefined){
                                                var id = newtile.image;
                                                if(tilesets[id] !== undefined){
                                                    newtile.image = tilesets[id];
                                                }
                                            }

                                            if(tile[10] !== undefined){
                                                newtile.bounds = {
                                                    x:tile[10],
                                                    y:tile[11],
                                                    width:tile[12],
                                                    height:tile[13]
                                                };
                                                var layer_tmp = self.colisionLayers[newtile.layer];
                                                var grid =  layer_tmp.getGrid();
                                                var rect = grid.get(indexA,indexB);
                                                rect.state = true;
                                            }

                                            tmp_map.setTile(indexA,indexB,newtile);
                                        }
                                        else{
                                            tmp_map.removeTile(indexA,indexB);
                                        }
                                    });
                                }
                            });
                        }
                    });
                    $scope.changeMapWidth(parseInt(data.width));
                    $scope.changeMapHeight(parseInt(data.height));
                    self.colisionLayers.forEach(function(layer){
                        layer.refresh();
                    });
                });

                var mapCanvas = self.getMapCanvas();

                var draw_callback =function(rect,context){
                    if(!rect.state){
                        context.strokeStyle = 'rgba(0,0,230,0.8)';
                        context.beginPath();
                        context.arc(rect.x+(rect.width/2), rect.y+(rect.height/2), 10, 0, 2 * Math.PI);
                        context.stroke();
                    }
                    else{
                        context.strokeStyle = 'rgba(230,0,0,1)';
                        context.beginPath();
                        context.moveTo(rect.x+5,rect.y+5);
                        context.lineTo(rect.x+rect.width-5,rect.y+rect.height-5);
                        context.stroke();
                        context.beginPath();
                        context.moveTo(rect.x+rect.width-5,rect.y+5);
                        context.lineTo(rect.x+5,rect.y+rect.height-5);
                        context.stroke();
                    }
                };

                for(var i =0;i<10;i++){
                    var layer = mapCanvas.createLayer({},CE.GridLayer);
                    var grid = layer.getGrid();
                    grid.set({sw:32, sh:32});
                    grid.apply({strokeStyle:'transparent', fillStyle:'transparent'});
                    grid.ondrawcallback(draw_callback);
                    layer.set({
                        opacity:0
                    });

                    self.colisionLayers[i] = layer;
                }


                tmp_map.eachTile(function(tile,i,j){
                    if(tile.bounds !== undefined && tile.bounds !== null){
                        if(self.colisionLayers[tile.layer] !== undefined){
                            layer = self.colisionLayers[tile.layer];
                            var grid = layer.getGrid();
                            var rect =  grid.get(i,j);
                            if(rect !== null){
                                rect.state = true;
                            }
                        }
                    }
                });

                self.colisionLayers.forEach(function(layer){
                    layer.refresh();
                });
            };


            self.initializeMap = function(){
                var mapCanvas = self.getMapCanvas();
                var grid = new CE.AbstractGrid({
                    sw:32,
                    sh:32
                });

                var layer = mapCanvas.getGridLayer();
                layer.setGrid(grid);
                layer.refresh();

                for(var i = 0; i < 10;i++){
                    $scope.layers[i] = mapCanvas.createLayer();
                }


                mapCanvas.getMouseReader().onmousemove(function(){
                    var reader = this;
                    if(reader.right){
                        var gridLayer =  mapCanvas.getGridLayer();
                        gridLayer.refresh();
                        self.getMap().draw($scope.layers,gridLayer.getVisibleArea());
                    }
                });

            };

            self.initializeTileset = function(){
                var tilesetCanvas = self.getTilesetCanvas();
                self.tilesetLayer = tilesetCanvas.createLayer({
                    name:'tileset-layer'
                });
                var layer = tilesetCanvas.getGridLayer();
                layer.set({
                    opacity:0.8
                });

                layer.refresh();
            };

            self.objectMap = function(map){
                var tiles = [];
                var tilesets = [];
                var events = [];

                map.eachTile(function(tile,i,j,layer){
                    if(tiles[i] === undefined){
                        tiles[i] = [];
                    }

                    if(tiles[i][j] === undefined){
                        tiles[i][j] = [];
                    }

                    if(tile !== null){
                        var src =  tile.image.src;
                        var id = tilesets.indexOf(src);
                        if(id === -1){
                            tilesets.push(src);
                            id = tilesets.length-1;
                        }

                        var new_tile = [
                            id,
                            tile.dWidth,
                            tile.dHeight,
                            tile.sWidth,
                            tile.sHeight,
                            tile.sx,
                            tile.sy,
                            tile.dx,
                            tile.dy,
                            tile.layer
                        ];


                        if(tile.bounds !== undefined){
                            new_tile[10] = tile.bounds.x;
                            new_tile[11] = tile.bounds.y;
                            new_tile[12] = tile.bounds.width;
                            new_tile[13] = tile.bounds.height;
                        }
                        tiles[i][j][layer] = new_tile;
                    }
                });

                return {
                    tilesets:tilesets,
                    events:events,
                    width:map.width,
                    height:map.height,
                    tile_w:map.tile_w,
                    tile_h:map.tile_h,
                    tiles:tiles
                };
            };

            $scope.createEvent = function(){

            };

            $scope.changeGraphic = function(url){
                $scope.modalVisible = false;
                $scope.tilesetData.graphic.imageData.url = url;
            };

            $scope.changeMapWidth = function(width){
                var mapCanvas = self.getMapCanvas();
                var map  = self.getMap();
                map.width = width;
                mapCanvas.applyToLayers({
                    width:map.tile_w*width
                });
                var gridLayer = mapCanvas.getGridLayer();
                gridLayer.getGrid().set({
                    width:map.tile_w*width
                });
                gridLayer.refresh();

                self.colisionLayers.forEach(function(layer){
                    layer.getGrid().set({
                        width:map.tile_w*width
                    });
                    layer.refresh();
                });

                map.draw($scope.layers,gridLayer.getVisibleArea());
            };

            $scope.changeMapHeight = function(height){
                var mapCanvas = self.getMapCanvas();
                var map = self.getMap();
                map.height = height;
                mapCanvas.applyToLayers({
                    height:map.tile_h*height
                });

                var gridLayer = mapCanvas.getGridLayer();
                gridLayer.getGrid().set({
                    height:map.tile_h*height
                });

                gridLayer.refresh();

                self.colisionLayers.forEach(function(layer){
                    layer.getGrid().set({
                        height:map.tile_h*height
                    });
                    layer.refresh();
                });


                map.draw($scope.layers,gridLayer.getVisibleArea());
            };

            $scope.$watch('tilesetData.graphic.imageData.url',function(newVal, oldVal){
                $timeout(function(){
                    if(newVal !== oldVal && self.tilesetLayer !== null){
                        ImageLoader.load(newVal,function(img){
                            $scope.tilesetData.graphic.image = img;
                            var tilesetImage = self.getTilesetCanvas();

                            tilesetImage.set({
                                viewX:0,
                                viewY:0
                            });

                            tilesetImage.applyToLayers({
                                width:img.width,
                                height:img.height
                            });

                            self.tilesetLayer.clear().drawImage(img,0,0);
                            var tilesetGridLayer =  tilesetImage.getGridLayer();


                            tilesetGridLayer.getGrid().set({
                                width:img.width,
                                height:img.height,
                                sw:img.width,
                                sh:img.height
                            });

                            tilesetGridLayer.refresh();


                            $timeout(function(){
                                var rows = Math.floor(img.height/32);
                                var cols = Math.floor(img.width/32);
                                rows = rows < 1?1:rows;
                                cols = cols < 1?1:cols;
                                $scope.tilesetData.graphic.rows = rows;
                                $scope.tilesetData.graphic.cols = cols;
                                $scope.changeRows(rows);
                                $scope.changeCols(cols);
                            });

                        });

                    }
                });
            });

            $scope.changeRows = function(val){
                var image = $scope.tilesetData.graphic.image;
                if(image !== null){
                    var map = self.getMap();
                    var layer = self.getTilesetCanvas().getGridLayer();
                    var layer2 = self.getMapCanvas().getGridLayer();
                    var height = image.height/val;
                    map.tile_h= height;
                    layer.getGrid().set({
                        sh:height
                    });
                    layer2.getGrid().set({
                        sh:height
                    });
                    layer.refresh();
                    layer2.refresh();
                }
            };

            $scope.changeCols = function(val){
                var image = $scope.tilesetData.graphic.image;
                if(image !== null){
                    var map = self.getMap();
                    var layer = self.getTilesetCanvas().getGridLayer();
                    var layer2 = self.getMapCanvas().getGridLayer();
                    var width = image.width/val;
                    map.tile_w = width;
                    layer.getGrid().set({
                        sw:width
                    });
                    layer2.getGrid().set({
                        sw:width
                    });
                    layer.refresh();
                    layer2.refresh();
                }
            };

            $scope.changeLayer = function(layer){
                $scope.currentLayer = layer;
                $scope.layers.forEach(function(layer_tmp,index){
                    layer_tmp.set({opacity:index === layer?1:0.3});
                });

                if($scope.mode === 'colision'){
                    self.colisionLayers.forEach(function(layer_tmp,index){
                        layer_tmp.set({opacity:index === layer?1:0});
                    });
                }

            };

            $scope.export = function(){
               console.log(JSON.stringify(self.objectMap(self.getMap())));
            };

            /*Canvas onde o mapa é renderizado*/
            self.getMapCanvas = function(){
                if(self.mapCanvas === null){
                    self.mapCanvas = CE.createEngine({
                        container:'#canvas-map',
                        width:600,
                        height:600,
                        draggable:true
                    },CE.CanvasEngineGrid);


                    var reader = self.mapCanvas.getMouseReader();
                    reader.onmousedown(MouseReader.LEFT,function(){
                        var reader = this;
                        var mapCanvas = self.getMapCanvas();
                        var map = self.getMap();
                        var pos = mapCanvas.getPosition(reader.lastDown.left);
                        var i = Math.floor(pos.y/map.tile_h);
                        var j = Math.floor(pos.x/map.tile_w);

                        switch($scope.mode){
                            case 'map':
                                var layer = $scope.layers[$scope.currentLayer];
                                var tile = {
                                    width:map.tile_w,
                                    height:map.tile_h
                                };

                                switch($scope.selectedTool){
                                    case 'pencil':
                                        if(self.selectedInterval !== null){
                                            if(self.selectedInterval.type === 'eyedropper'){
                                                tile = jQuery.extend(true, {},self.selectedInterval);
                                                tile.layer = $scope.currentLayer;
                                                tile.dx = j*map.tile_w;
                                                tile.dy = i*map.tile_h;

                                                layer.clearRect({
                                                    x:tile.dx,
                                                    y:tile.dy,
                                                    width:tile.dWidth,
                                                    height:tile.dHeight
                                                });


                                                layer.image(tile);
                                                map.setTile(i,j,tile);
                                            }
                                            else{
                                                var image = $scope.tilesetData.graphic.image;
                                                var interval = self.selectedInterval;
                                                tile.sx = map.tile_w*interval.sj;
                                                tile.sy = map.tile_h*interval.si;
                                                tile.dx = map.tile_w*j;
                                                tile.dy = map.tile_h*i;
                                                tile.image = image;
                                                tile.dWidth = map.tile_w;
                                                tile.dHeight = map.tile_h;
                                                tile.sWidth = map.tile_w;
                                                tile.sHeight = map.tile_h;
                                                tile.layer = $scope.currentLayer;
                                                layer.clearRect({
                                                    x:tile.dx,
                                                    y:tile.dy,
                                                    width:tile.dWidth,
                                                    height:tile.dHeight
                                                });
                                                layer.image(tile);
                                                map.setTile(i,j,tile);
                                            }
                                        }
                                        break;
                                    case 'eraser':
                                        tile.x = map.tile_w*j;
                                        tile.y = map.tile_h*i;
                                        layer.clearRect(tile);
                                        map.removeTile(i,j,$scope.currentLayer);
                                        break;
                                    case 'eyedropper':
                                        tile = map.getTile(i,j,$scope.currentLayer);
                                        tile = jQuery.extend(true, {}, tile);
                                        tile.type = 'eyedropper';
                                        self.selectedInterval = tile;

                                }
                                break;
                            case 'colision':
                                var tile = map.getTile(i,j,$scope.currentLayer);
                                if(tile !== null){
                                    var layer = self.colisionLayers[$scope.currentLayer];
                                    var state = false;
                                    if(tile.bounds === undefined){
                                        tile.bounds = Utils.getTrimmedBounds(tile.image,tile);
                                        state = true;
                                    }
                                    else{
                                        delete tile.bounds;
                                    }


                                    layer.getGrid().get(i,j).state = state;
                                    layer.refresh();
                                }
                                break;
                        }
                    });

                    reader.onmousemove(function(e){
                        var reader = this;
                        /*start hover square*/
                        var mapCanvas = self.getMapCanvas();
                        var pos = mapCanvas.getPosition(reader.lastMove);
                        var map = self.getMap();
                        var x = Math.floor(pos.x/map.tile_w)*map.tile_w;
                        var y = Math.floor(pos.y/map.tile_h)*map.tile_h;
                        var grid_layer = mapCanvas.getGridLayer();
                        grid_layer.getGrid().setCheckedArea({
                            x:x,
                            y:y,
                            width:map.tile_w,
                            height:map.tile_h
                        });
                        grid_layer.refresh();
                        /*end hover square*/
                        switch($scope.mode){
                            case 'map':
                                var i, j;
                                if(reader.left){
                                    var interval,area_interval,col,row;
                                    var layer = $scope.layers[$scope.currentLayer];

                                    switch($scope.selectedTool){
                                        case 'pencil':
                                            if(self.selectedInterval !== null){
                                                interval = self.selectedInterval;
                                                var area = mapCanvas.getDrawedArea();
                                                area_interval = map.getAreaInterval(area);

                                                if(self.selectedInterval.type === 'eyedropper'){
                                                    tile = jQuery.extend(true, {},self.selectedInterval);
                                                    delete tile.type;
                                                    for(i = area_interval.si;i <= area_interval.ei;i++){
                                                        for(j = area_interval.sj; j <= area_interval.ej;j++){
                                                            tile.dx = j*map.tile_w;
                                                            tile.dy = i*map.tile_h;
                                                            tile.layer = $scope.currentLayer;
                                                            layer.clearRect({
                                                                x:tile.dx,
                                                                y:tile.dy,
                                                                width:map.tile_w,
                                                                height:map.tile_h
                                                            });

                                                            layer.image(tile);
                                                            map.setTile(i,j,tile);
                                                        }
                                                    }
                                                }
                                                else{
                                                    var image = $scope.tilesetData.graphic.image;
                                                    for(i = area_interval.si,row=interval.si;i <= area_interval.ei;i++){
                                                        for(j = area_interval.sj,col=interval.sj; j <= area_interval.ej;j++){
                                                            x = j*map.tile_w;
                                                            y = i*map.tile_h;

                                                            var tile = {
                                                                image:image,
                                                                dWidth:map.tile_w,
                                                                dHeight:map.tile_h,
                                                                sWidth:map.tile_w,
                                                                sHeight:map.tile_h,
                                                                sx:col*map.tile_w,
                                                                sy:row*map.tile_h,
                                                                dx:x,
                                                                dy:y,
                                                                layer:$scope.currentLayer,
                                                                colision:false
                                                            };

                                                            layer.clearRect({
                                                                x:x,
                                                                y:y,
                                                                width:map.tile_w,
                                                                height:map.tile_h
                                                            });

                                                            layer.image(tile);
                                                            map.setTile(i,j,tile);
                                                            col++;
                                                            if(col > interval.ej){
                                                                col = interval.sj;
                                                            }
                                                        }
                                                        row++;
                                                        if(row > interval.ei){
                                                            row = interval.si;
                                                        }
                                                    }
                                                }
                                            }

                                            break;
                                        case 'eraser':
                                            area_interval = map.getAreaInterval({x:pos.x,y:pos.y,width:1,height:1});
                                            for(i = area_interval.si;i <= area_interval.ei;i++){
                                                for(j = area_interval.sj; j <= area_interval.ej;j++){
                                                    x = j*map.tile_w;
                                                    y = i*map.tile_h;

                                                    layer.clearRect({
                                                        x:x,
                                                        y:y,
                                                        width:map.tile_w,
                                                        height:map.tile_h
                                                    });
                                                    map.removeTile(i,j,$scope.currentLayer);
                                                }
                                            }
                                    }
                                }
                                break;
                        }
                    });
                }
                return self.mapCanvas;
            };

            /*Tileset Canvas*/
            self.getTilesetCanvas = function(){
                if(self.tilesetCanvas === null){
                    self.tilesetCanvas = CE.createEngine({
                        container:'#tileset',
                        width:520,
                        height:520,
                        selectable:true,
                        draggable:true,
                        multiSelect:true
                    },CE.CanvasEngineGrid);


                    self.tilesetCanvas.onAreaSelect(function(area,grid){
                        var reader = this.getMouseReader();
                        if(reader.left){
                            var rectSets = grid.getRectsFromArea(area);
                            var interval = grid.getAreaInterval(area);
                            grid.apply({
                                fillStyle:'transparent',
                                state:0
                            });
                            rectSets.forEach(function(rectSet){
                                rectSet.set({
                                    fillStyle:'rgba(0,0,100,0.5)',
                                    state:1
                                });
                            });
                            self.selectedInterval = interval;
                        }
                        else{
                            var rects = grid.getRectsFromArea(area);
                            if(rects.length > 0){
                                var rectSet = rects[0];
                                grid.apply({
                                    fillStyle:'transparent'
                                },function(){
                                    return this.state != 1;
                                });
                                rectSet.set({
                                    fillStyle:'rgba(0,0,100,0.5)'
                                });
                            }
                        }
                    });
                }
                return self.tilesetCanvas;
            };

            self.getMap = function(){
                var self = this;
                if($scope.tmp_map === null){
                    var mapCanvas = self.getMapCanvas();
                    $scope.tmp_map = new CE.Map({
                        sw:32,
                        sh:32,
                        width:$scope.map.width,
                        height:$scope.map.height
                    });
                }
                return $scope.tmp_map;
            };

            $scope.selectTool = function(tool){
                $scope.selectedTool = tool;
            };

            $scope.showLayers = function(){
                $scope.layers.forEach(function(layer){
                    layer.set({
                        opacity:1
                    });
                });
            };

            $scope.save = function(){
                var json = JSON.stringify(self.objectMap(self.getMap()));
                json = LZString.compressToUTF16(json);

                $http({
                    method:'POST',
                    url:URLS.BASE_URL+'maps/update',
                    data:{
                        id:$scope.map._id,
                        map:json
                    },
                    withCredentials:true
                }).then(function(response){
                    if(response.data.success){
                        console.log('mapa atualizado!');
                    }
                    else{
                        console.log(response.data);
                    }
                },function(error){
                    console.log(error);
                });
            };

            self.loadMap = function(name,success,error){
                $http({
                    method:'GET',
                    url:URLS.BASE_URL+'maps/load',
                    params:{
                        name:name
                    },
                    withCredentials:true
                }).then(function(response){
                    if(response.data.success){
                        $scope.map = response.data.map;
                        $timeout(function(){
                            success();
                        });
                    }
                    else{
                        error();
                    }
                },function(){
                    error();
                });
            };

            $scope.changeMode = function(mode){
                $scope.mode = mode;

                if(mode ==='colision'){
                    self.colisionLayers.forEach(function(layer,index){
                        layer.set({opacity:index === $scope.currentLayer?1:0});
                    });
                }
                else{
                    self.colisionLayers.forEach(function(layer,index){
                        layer.set({opacity:0});
                    });
                }
            };
        }
    ]
);
