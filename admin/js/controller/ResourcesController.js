app.controller('ResourcesController',['$rootScope','$http','$timeout','URLS','ResourceService','$localStorage','TaskService','$stateParams',function($scope,$http,$timeout,URLS,ResourceService,$localStorage,TaskService,$stateParams){
    $scope.modalVisible = false;
    $scope.pagination = {
        current:1
    };

    $scope.resources = [];
    $scope.metadata = {
        files:[],
        images:[]
    };

    $scope.resource_type = $stateParams.type;
    $scope.resource_title =  $stateParams.type;

    $scope.showModal = function(){
        $scope.modalVisible = true;
        $scope.resource = null;
    };

    $scope.hideModal = function(){
        $scope.modalVisible = false;
    };

    $scope.remove = function(id) {
        ResourceService.remove($scope.resource_type,id);
        $scope.changePage($scope.pagination.current);
    };

    $scope.load = function(){
        ResourceService.loadResourcePage($scope.resource_type,1,8,function(resources){
            $scope.resources = resources;
        });
    };

    $scope.changePage = function(page){
        ResourceService.loadResourcePage($scope.resource_type,page,8,function(resources){
            $scope.resources = resources;
        });
    };

    $scope.afterEach = function(data){
        if(data.success){
            $scope.storage.resources[data.type].unshift(data.doc);
            $scope.changePage($scope.pagination.current);
        }
    };

    $scope.sincronize = function(){
        var task = {
            action:'SINCRONIZE_RESOURCES',
            date:new Date(),
            priority:2,
            data:{
                name:$scope.resource_type
            }
        };
        TaskService.add(task);
    };

    $scope.storage = $localStorage;
}]);