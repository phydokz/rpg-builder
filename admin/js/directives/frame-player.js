app.directive('framePlayer',['$timeout',function($timeout){
    return {
        restrict:'E',
        templateUrl:'templates/Elements/frame_player.html',
        scope:{
            title:'@title',
            ngAdd:'&',
            ngSelect:'&',
            ngRemove:'&',
            playCallback:'&',
            pauseCallback:'&',
            stopCallback:'&',
            frames:'=',
            selected:'='
        },
        link:function(scope){
            scope.add = function(){
                $timeout(function(){
                    scope.ngAdd()();
                });
            };

            scope.remove = function(index){
                scope.ngRemove()(index);
            };

            scope.select = function(index){
                scope.ngSelect()(index);
            };

            scope.play = function(){
                if(typeof scope.playCallback() === 'function'){
                    scope.playCallback()();
                }
            };

            scope.pause = function(){
                if(typeof scope.pauseCallback() === 'function'){
                    scope.pauseCallback()();
                }
            };

            scope.stop = function(){
                if(typeof scope.stopCallback() === 'function'){
                    scope.stopCallback()();
                }
            };

            scope.$watch('frames',function(newVal, oldVal){
                console.log(newVal);
            });
        }
    };
}]);