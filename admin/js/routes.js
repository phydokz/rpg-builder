app.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('panel',{
        url:'/panel',
        templateUrl:'templates/Panel/index.html',
        controller:'PanelController as Panel',
        data:{
            authorizedRoles:['admin']
        }
    }).state('resources', {
        url: '/resources',
        templateUrl: 'templates/Pages/resources.html',
        data:{
            authorizedRoles:['admin']
        }
    }).state('tasks',{
        url:'/tasks',
        templateUrl: 'templates/Pages/tasks.html',
        data:{
            authorizedRoles:['admin']
        }
    }).state('resources-list',{
        url:'/resources/:type',
        templateUrl:'templates/Pages/resources_list.html',
        controller:'ResourcesController',
        data:{
            authorizedRoles:['admin']
        }
    }).state('maps', {
        url: '/maps',
        templateUrl: 'templates/Pages/maps.html',
        controller: 'MapsController as MapCtrl',
        data:{
            authorizedRoles:['admin']
        }
    }).state('groups',{
        url:'/groups',
        templateUrl:'templates/Pages/groups.html',
        controller:'GroupsController',
        data:{
            authorizedRoles:['admin']
        }
    }).state('animations', {
        url: '/animations',
        templateUrl: 'templates/Pages/animations.html',
        controller: 'AnimationsController',
        data:{
            authorizedRoles:['admin']
        }
    }).state('map-editor',{
        url: '/map-editor/:name',
        templateUrl: 'templates/Pages/map-editor.html',
        controller: 'MapEditorController as MapEditorCtrl',
        data:{
            authorizedRoles:['admin']
        }
    }).state('group-editor',{
        url:'/group-editor/:name',
        templateUrl: 'templates/Pages/group-editor.html',
        controller: 'GroupEditorController',
        data:{
            authorizedRoles:['admin']
        }
    }).state('animation-editor', {
        url: '/animation-editor/:name',
        templateUrl: 'templates/Pages/animation-editor.html',
        controller: 'AnimationEditorController',
        data:{
            authorizedRoles:['admin']
        }
    }).state('login',{
        url:'/login',
        templateUrl:'templates/Elements/login_form.html',
        controller:'UserController',
        data:{
            authorizedRoles:['public']
        }
    }).state('init',{
        url:'/',
        redirectTo:'panel'
    });
}]);
