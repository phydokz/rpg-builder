var IdGenerator = {
    used:[],
    getUniqueId: function () {
        var self = this;
        var id = self.generateUUID();
        while(self.used[id] !== undefined){
            id = self.generateUUID();
        }
        self.used[id] = true;
        return id;
    },
    drop: function (id) {
        var self = this;
        if(self.used[id] !== undefined){
            delete self.used[id];
        }
    },
    generateUUID:function() {
        var d = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
    }
};
